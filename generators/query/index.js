const ComponentGenerator = require("../../helpers/componentGenerator");

const settings = {
  base: "queries",
  message: "Enter type of the query",
};

class QueryGenerator extends ComponentGenerator {
  constructor(args, opts) {
    super(args, opts, settings);
  }

  async promptingQueryName() {
    // SUPER-IMPORTANT: Await - Otherwise "Writing" methods are executed next
    await this.promptingName();
  }

  async promptingDatasource() {
    const answers = await this.prompt([
      {
        name: "datasource",
        type: "input",
        message: "Enter datasource which the query should use",
        validate(ds) {
          return !!ds;
        }
      }
    ]);

    this.datasource = answers["datasource"];
  }

  configuringComponent() {
    this.configuringPaths();
  }

  writingComponent() {
    this.writingComponents();
    this.writingRequirements();
  }

  writingQuery() {
    this.fs.copyTpl(
      this.templatePath("./query.js"),
      this._private_destination(),
      {
        type: this._private_name(),
        datasource: this.datasource,
      }
    );
  }
}

module.exports = QueryGenerator;