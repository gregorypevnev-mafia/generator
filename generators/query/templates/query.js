const type = "<%= type %>";
const datasource = "<%= datasource %>";

const handler = datasource => ({ }) => async () => {

};

module.exports = {
  type,
  datasource,
  handler,
};