const Generator = require("yeoman-generator");
const { paramCase } = require("param-case");

class AuthGenerator extends Generator {
  constructor(args, opts) {
    super(args, opts);

    this.conflicter.force = true;

    this.argument("event-name", { type: String, required: true });

    this.name = paramCase(this.options["event-name"]);
  }

  async promptingEventTopic() {
    const answers = await this.prompt([
      {
        name: "topic",
        type: "input",
        message: "Which topic should the event be sent to?",
        validate(type) {
          return !!type;
        },
        filter(type) {
          return paramCase(String(type));
        }
      }
    ]);

    this.topic = answers["topic"];
  }

  writingEvent() {
    const eventsFile = this.destinationPath("./src/app/events.json");

    const events = this.fs.readJSON(eventsFile, {});

    this.fs.writeJSON(eventsFile, {
      ...events,
      [this.name]: {
        topic: this.topic,
      },
    })
  }
}

module.exports = AuthGenerator;
