const Generator = require("yeoman-generator");
const { MONGO_DS, REDIS_DS, SERVICE_DS, SQL_DS } = require("./utils/types");
const { questionsForType } = require("./utils/questions");
const { addDatasource } = require("./utils/requirements");

const DATASOURCES_PATH = "./src/config/components/datasources";
const MIGRATIONS_PATH = "./src/resources/migrations";

class DatasourceGenerator extends Generator {
  constructor(args, opts) {
    super(args, opts);

    this.argument("datasource", { type: String, required: true });

    this.conflicter.force = true;
  }

  initializing() {
    this.datasource = this.options["datasource"];
    this.type = null;
    this.details = {};
  }

  async promptingType() {
    const answers = await this.prompt([
      {
        name: "type",
        type: "list",
        message: "Enter type of the datasource",
        choices: [
          {
            name: "SQL",
            value: SQL_DS
          },
          {
            name: "MongoDB",
            value: MONGO_DS
          },
          {
            name: "Redis",
            value: REDIS_DS
          },
          {
            name: "Microservice",
            value: SERVICE_DS
          }
        ]
      }
    ]);

    this.type = answers["type"];
  }

  async promptingDetails() {
    this.details = await this.prompt(questionsForType(this.type));
  }

  writingDatasource() {
    this.fs.copyTpl(
      this.templatePath(`${this.type}.js`),
      this.destinationPath(`${DATASOURCES_PATH}/${this.datasource}.js`),
      this.details
    );
  }

  writingRequirements() {
    const modulePath = `${DATASOURCES_PATH}/index.js`;

    this.fs.write(modulePath, addDatasource(this.fs.read(modulePath), this.datasource));
  }

  writingMigration() {
    if (this.type !== SQL_DS) return; // Only using migrations for Relational Databases

    const migrationFilePath = `${MIGRATIONS_PATH}/${this.datasource}/knexfile.js`;
    const migrationsDirectoryPath = `${MIGRATIONS_PATH}/${this.datasource}/migrations`;

    if (this.fs.exists(migrationFilePath)) return;

    this.fs.copyTpl(
      this.templatePath("migration.js"),
      this.destinationPath(migrationFilePath),
      this.details
    );

    this.fs.write(this.destinationPath(`${migrationsDirectoryPath}/.gitkeep`), "");
  }
}

module.exports = DatasourceGenerator;