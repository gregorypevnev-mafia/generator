module.exports = {
  type: "sql",
  config: {
    database: "<%= database %>",
    host: "<%= host %>",
    port: Number("<%= port %>"),
    user: "<%= username %>",
    password: "<%= password %>"
  }
};