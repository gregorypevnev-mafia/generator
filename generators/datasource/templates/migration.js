// Additional: Using environment variables
const connectionConfiguration = {
  database: "<%= database %>",
  host: "<%= host %>",
  port: Number("<%= port %>"),
  user: "<%= username %>",
  password: "<%= password %>"
};

module.exports = {
  client: "pg",
  connection: connectionConfiguration,
  pool: {
    min: 2,
    max: 10
  }
};
