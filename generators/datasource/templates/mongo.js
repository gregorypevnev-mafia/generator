module.exports = {
  type: "mongo",
  config: {
    host: "<%= host %>",
    port: Number("<%= port %>"),
    user: "<%= username %>",
    password: "<%= password %>",
    database: "<%= database %>",
    <% if(rs) { %>replicaSet: "<%= rs %>" <% } %>
  }
};
