module.exports = {
  type: "service",
  config: {
    url: "<%= url %>",
    timeout: 1000,
    circuitBreaker: {
      failureCount: 3,
      failureTimeout: 5000,
      retriesCount: 2,
      retriesTimeout: 100,
    }
  }
};