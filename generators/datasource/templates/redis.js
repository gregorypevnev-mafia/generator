module.exports = {
  type: "redis",
  config: {
    port: Number("<%= port %>"),
    host: "<%= host %>",
    <% if(password) { %>password: "<%= password %>" <% } %>
  }
};
