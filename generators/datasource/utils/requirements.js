const esprima = require("esprima");
const escodegen = require("escodegen");

const generateDatasourceRequirement = name => ({
  "type": "Property",
  "key": {
    "type": "Identifier",
    "name": name
  },
  "computed": false,
  "value": {
    "type": "CallExpression",
    "callee": {
      "type": "Identifier",
      "name": "require"
    },
    "arguments": [
      {
        "type": "Literal",
        "value": `./${name}`,
        "raw": `\"./${name}\"`
      }
    ]
  },
  "kind": "init",
  "method": false,
  "shorthand": false
});

const addDatasource = (main, datasourceRequirement) => {
  const datasources = esprima.parseModule(main);

  datasources.body[0].expression.right.properties.push(
    generateDatasourceRequirement(datasourceRequirement)
  );

  return escodegen.generate(datasources);
}

module.exports = { addDatasource };
