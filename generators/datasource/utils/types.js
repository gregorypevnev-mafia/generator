const MONGO_DS = "mongo";
const SQL_DS = "sql";
const REDIS_DS = "redis";
const SERVICE_DS = "service";

module.exports = {
  MONGO_DS,
  SQL_DS,
  REDIS_DS,
  SERVICE_DS,
};
