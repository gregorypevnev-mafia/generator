const { MONGO_DS, REDIS_DS, SERVICE_DS, SQL_DS } = require("./types");

const URL_PATTERN = /^https?\:\/\/[a-z0-9\-]+(\:[0-9]{1,5})?(\/[a-z0-9\-\/]*)?$/;

const HOST_QUESTION = {
  name: "host",
  type: "input",
  message: "Enter host",
  default: "localhost",
  validate(host) {
    return !!host;
  }
};

const PORT_QUESTION = defaultPort => ({
  name: "port",
  type: "number",
  message: "Enter port",
  default: defaultPort,
  validate(port) {
    return port > 0;
  }
});

const DATABASE_QUESTION = name => ({
  name: "database",
  type: "input",
  message: "Enter database",
  default: name,
  validate(host) {
    return !!host;
  }
});

const USERNAME_QUESTION = {
  name: "username",
  type: "input",
  message: "Enter username",
  default: "user",
  validate(username) {
    return !!username;
  }
};

const PASSWORD_QUESTION = (isRequired = true) => ({
  name: "password",
  type: "password",
  message: "Enter password",
  validate(password) {
    return !isRequired || !!password;
  }
});

const RS_QUESTION = {
  name: "rs",
  type: "input",
  message: "Enter name of the replica set",
  default: "",
};

const URL_QUESTION = {
  name: "url",
  type: "input",
  message: "Enter URL of the service",
  validate(url) {
    return !!String(url).match(URL_PATTERN);
  }
};

const QUESTIONS = {
  [SQL_DS]: [
    HOST_QUESTION,
    PORT_QUESTION(5432),
    DATABASE_QUESTION("data"),
    USERNAME_QUESTION,
    PASSWORD_QUESTION(true)
  ],
  [MONGO_DS]: [
    HOST_QUESTION,
    PORT_QUESTION(27017),
    DATABASE_QUESTION("admin"),
    USERNAME_QUESTION,
    PASSWORD_QUESTION(true),
    RS_QUESTION
  ],
  [REDIS_DS]: [
    HOST_QUESTION,
    PORT_QUESTION(6379),
    PASSWORD_QUESTION(false)
  ],
  [SERVICE_DS]: [URL_QUESTION],
};

const questionsForType = type => {
  const questions = QUESTIONS[type];

  if (questions) return questions;

  throw new Error("Invalid type of a datasource");
};

module.exports = { questionsForType };