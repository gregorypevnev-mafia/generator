const Generator = require("yeoman-generator");

class DevOpsGenerator extends Generator {
  constructor(args, opts) {
    super(args, opts);

    this.conflicter.force = true;

    this.argument("service-name", { type: String, required: true });
  }

  writingIndividual() {
    this.fs.copyTpl(
      this.templatePath("./individual/docker-compose.yml"),
      this.destinationPath("./docker-compose.yml"),
      { name: String(this.options["service-name"]) }
    );
  }

  writingShared() {
    this.fs.copy(
      this.templatePath("./shared"),
      this.destinationPath("./"),
    );
  }

  writingSpecial() {
    this.fs.copy(
      this.templatePath("./special/dockerignore"),
      this.destinationPath("./.dockerignore"),
    );
    this.fs.copy(
      this.templatePath("./special/gitignore"),
      this.destinationPath("./.gitignore"),
    );
    this.fs.copy(
      this.templatePath("./special/gitlab-ci.yml"),
      this.destinationPath("./.gitlab-ci.yml"),
    );
  }

  writingTestCommand() {
    const PACKAGE_PATH = this.destinationPath("package.json");

    const packageConfig = this.fs.readJSON(PACKAGE_PATH);

    packageConfig.scripts["test"] = "jest --passWithNoTests";

    this.fs.writeJSON(PACKAGE_PATH, packageConfig);
  }
}

module.exports = DevOpsGenerator;