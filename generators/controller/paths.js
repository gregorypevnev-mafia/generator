const PATH_PATTERN = /^(\/?([A-Za-z0-9\-]+))*\/?$/;

const isValidPath = path => !!String(path).match(PATH_PATTERN);

const formatPath = path => {
  if (path.startsWith("/")) return path;

  return `/${path}`;
};

module.exports = { isValidPath, formatPath };