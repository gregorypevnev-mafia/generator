const ComponentGenerator = require("../../helpers/componentGenerator");
const { isValidPath, formatPath } = require("./paths");

const settings = {
  base: "api/controllers",
};

class ControllerGenerator extends ComponentGenerator {
  _private_isApiReady() {
    return this.fs.exists(this.destinationPath("src/app/api/controllers/index.js"));
  }

  constructor(args, opts) {
    super(args, opts, settings);
  }

  initializingApi() {
    if (!this._private_isApiReady())
      throw new Error("API not initialized");
    // Do NOT compose - Will not write requirement
  }

  async promptingControllerDetails() {
    const answers = await this.prompt([
      {
        name: "path",
        type: "input",
        message: "Enter path for the controller:",
        validate(path) {
          return isValidPath(path);
        },
        filter(path) {
          return formatPath(path);
        },
      },
      {
        name: "operations",
        type: "checkbox",
        message: "Which operations should be supported by the controller?",
        choices: [
          {
            name: "List",
            value: "list",
            checked: true
          },
          {
            name: "Create",
            value: "create",
            checked: false
          },
          {
            name: "Find",
            value: "find",
            checked: false
          },
          {
            name: "Remove",
            value: "remove",
            checked: false
          },
          {
            name: "Update",
            value: "update",
            checked: false
          }
        ]
      },
      {
        name: "auth",
        type: "confirm",
        message: "Authenticating users?",
        default: false,
      }
    ]);

    this.details = {
      ...answers,
      private: false,
      protected: false,
    }
  }

  async promptingControllerAuthentication() {
    if (!this.details.auth) return;

    const answers = await this.prompt([
      {
        name: "private",
        type: "confirm",
        message: "Is authentication required?",
        default: false,
      }
    ]);

    this.details.private = answers["private"];
  }

  async promptingControllerAuthorization() {
    if (!this.details.private) return;

    const answers = await this.prompt([
      {
        name: "protected",
        type: "confirm",
        message: "Is authorization required?",
        default: false,
      }
    ]);

    this.details.protected = answers["protected"];
  }

  configuringComponent() {
    this.configuringPaths();
  }

  writingController() {
    this.fs.copyTpl(
      this.templatePath("./controller.js"),
      this._private_destination(),
      this.details,
    );
  }
}

module.exports = ControllerGenerator;
