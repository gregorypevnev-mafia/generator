<% if (operations.includes("list")) { %>const list = ({ }) => async (req, res) => {
  return res.status(200).json({});
};<% } %>
<% if (operations.includes("create")) { %>const create = ({ }) => async (req, res) => {
  return res.status(201).json({});
};<% } %>
<% if (operations.includes("find")) { %>const find = ({ }) => async (req, res) => {
  const id = String(req.params.id);

  return res.status(200).json({ id });
};<% } %>
<% if (operations.includes("remove")) { %>const remove = ({ }) => async (req, res) => {
  const id = String(req.params.id);

  return res.status(204).send();
};<% } %>
<% if (operations.includes("update")) { %>const update = ({ }) => async (req, res) => {
  const id = String(req.params.id);

  return res.status(200).json({});
};<% } %>

  module.exports = {
  path: "<%= path %>",
    middleware: [],
      auth: <%= auth %>,
        private: <%= private %>,
          protected: <%= protected %>,
            use: [
    <% if (operations.includes("list")) { %> {
    path: "/",
    middleware: [],
    use: {
      method: "GET",
      controller: list,
    }
  },<% } if (operations.includes("create")) { %> {
    path: "/",
    middleware: [],
    use: {
      method: "POST",
      controller: create,
    }
  },<% } if (operations.includes("find")) { %> {
    path: "/:id",
    middleware: [],
    use: {
      method: "GET",
      controller: find,
    }
  },<% } if (operations.includes("remove")) { %> {
    path: "/:id",
    middleware: [],
    use: {
      method: "DELETE",
      controller: remove,
    }
  },<% } if (operations.includes("update")) { %> {
    path: "/:id",
    middleware: [],
    use: {
      method: "PUT",
      controller: update,
    }
  },<% } %>
],
};
