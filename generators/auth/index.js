const Generator = require("yeoman-generator");

class AuthGenerator extends Generator {
  writingMessaging() {
    this.fs.copy(
      this.templatePath("./auth"),
      this.destinationPath("./src/app/auth")
    );
  }
}

module.exports = AuthGenerator;
