const formatUser = ({ id, password, ...details }) => ({ id, ...details }); // Stripping out password

exports.findUser = ({ }) => async ({ }) => {
  return formatUser(user);
};

exports.createUser = ({ }) => async data => {
  return formatUser(user);
};

exports.detailsForUser = ({ }) => user => formatUser(user); // No additional info

