const hasPermissions = (user, details) => !!user && !!details; // Could call another service or use Roles / Permissions logic

exports.verifyUserPermissions = ({ }) => (details, user) => {
  return hasPermissions(user, details);
};
