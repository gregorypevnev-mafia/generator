const Generator = require("yeoman-generator");

class ApiGenerator extends Generator {
  writingApi() {
    this.fs.copy(
      this.templatePath("./api"),
      this.destinationPath("./src/app/api")
    );
  }
}

module.exports = ApiGenerator;
