const Generator = require("yeoman-generator");

class ServerGenerator extends Generator {
  writingMessaging() {
    this.fs.copy(
      this.templatePath("./server"),
      this.destinationPath("./src/app/server")
    );
  }
}

module.exports = ServerGenerator;
