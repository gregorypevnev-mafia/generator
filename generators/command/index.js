const ComponentGenerator = require("../../helpers/componentGenerator");

const settings = {
  base: "commands",
  message: "Enter type of the command",
};

class CommandGenerator extends ComponentGenerator {
  constructor(args, opts) {
    super(args, opts, settings);
  }

  async promptingCommandName() {
    // SUPER-IMPORTANT: Await - Otherwise "Writing" methods are executed next
    await this.promptingName();
  }

  configuringComponent() {
    this.configuringPaths();
  }

  writingComponent() {
    this.writingComponents();
    this.writingRequirements();
  }

  writingCommand() {
    this.fs.copyTpl(
      this.templatePath("./command.js"),
      this._private_destination(),
      { type: this._private_name() }
    );
  }
}

module.exports = CommandGenerator;