const ComponentGenerator = require("../../helpers/componentGenerator");

const settings = {
  base: "api/middleware",
  message: "Enter name of the middleware",
};

class MiddlewareGenerator extends ComponentGenerator {
  _private_isApiReady() {
    return this.fs.exists(this.destinationPath("src/app/api/middleware/index.js"));
  }

  constructor(args, opts) {
    super(args, opts, settings);
  }

  initializingApi() {
    if (!this._private_isApiReady())
      throw new Error("API not initialized");
    // Do NOT compose - Will not write requirement
  }

  async promptingMiddlewareName() {
    // SUPER-IMPORTANT: Await - Otherwise "Writing" methods are executed next
    await this.promptingName();
  }

  configuringComponent() {
    this.configuringPaths();
  }

  writingComponent() {
    this.writingComponents();
    this.writingRequirements();
  }

  writingMiddleware() {
    this.fs.copyTpl(
      this.templatePath("./middleware.js"),
      this._private_destination(),
      { name: this._private_name() }
    );
  }
}

module.exports = MiddlewareGenerator;