const name = "<%= name %>";

const handler = ({ }) => ({ }) => async (req, res, next) => {
  return next();
};

module.exports = {
  name,
  handler,
};