const Generator = require("yeoman-generator");
const { questionsForType } = require("../datasource/utils/questions");

const DEFAULT_PORT = 3000;
const CONFIG_FILES = ["info", "core", "server"];

class AppGenerator extends Generator {
  constructor(args, opts) {
    super(args, opts);

    this.argument("service-name", { type: String, required: true });
  }

  initializing() {
    this.data = {
      name: this.options["service-name"],
      port: DEFAULT_PORT,
    };
  }

  async promptingServer() {
    const { port } = await this.prompt([
      {
        name: "port",
        type: "number",
        message: "Enter port for running the server",
        default: DEFAULT_PORT,
        validate(port) {
          return port > 0;
        }
      }
    ]);

    this.data.port = port;
  }

  async promptingKafka() {
    const { kafka } = await this.prompt([
      {
        name: "kafka",
        type: "input",
        message: "Enter connection string for Kafka Cluster",
        default: "localhost:9092"
      }
    ]);

    this.data.kafka = kafka;
  }

  async promptingTasks() {
    const { tasks } = await this.prompt([
      {
        name: "tasks",
        type: "confirm",
        message: "Is service using background jobs and tasks?",
        default: false
      }
    ]);

    this.data.tasks = tasks;
  }

  async promptingRedisStore() {
    if (!this.data.tasks) return;

    this.log("Enter Redis connection details for Task-Store");

    this.data.redis = await this.prompt(questionsForType("redis"));
  }

  writingPackage() {
    // Blocking -> OK to simply run
    this.fs.copyTpl(
      this.templatePath("./package.json"),
      this.destinationPath("./package.json"),
      this.data
    );
  }

  writingSource() {
    // Copying an entire directory - CANNOT supply template (Would have been too easy though)
    this.fs.copy(
      this.templatePath("./src"),
      this.destinationPath("./src"),
    );
  }

  writingConfig() {
    CONFIG_FILES.forEach(configFile => {
      this.fs.copyTpl(
        this.templatePath(`./config/${configFile}.js`),
        this.destinationPath(`./src/config/components/${configFile}.js`),
        this.data
      );
    });
  }

  installModules() {
    this.npmInstall([
      // General
      "config",
      "debug",
      // API
      "body-parser",
      "cors",
      "express",
      "http-proxy-middleware",
      "jsonwebtoken",  // Always used in APIs in one way or another
      "morgan",
      "multer",
      // Datasources
      "axios",
      "promise-retry",
      "knex",
      "pg",
      "mongodb",
      "async-redis",
      "redis",
      // Validation
      "yup",
      // // Authentication
      "bcrypt",
      // // Events
      "kafka-node",
      // // Messaging
      "ws",
      // Tasks
      "bull",
      // // Additional
      "uuid",
      "moment",
    ]);

    // Development-Dependencies

    this.npmInstall([
      "jest",
      "rewire",
    ], { "save-dev": true });
  }
}

module.exports = AppGenerator;