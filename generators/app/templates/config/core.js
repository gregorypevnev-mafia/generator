module.exports = {
  events: {
    mq: {
      id: "<%= name %>",
      host: "<%= kafka %>",
    },
  },

  <% if (tasks) { %> tasks: {
  queue: {
    name: "<%= name %>",
      config: {
        <% if (redis) { %> redis: {
        port: Number("<%= redis.port %>"),
          host: "<%= redis.host %>",
          <% if (redis.password) { %> password: "<%= redis.password %>" <% } %>
        }<% } %>
      }
  }
}<% } %>
<% if (!tasks) { %> tasks: { queue: null } <% } %>
}
