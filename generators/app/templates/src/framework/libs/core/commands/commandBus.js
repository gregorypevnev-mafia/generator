const debug = require("debug");

const commandLogger = debug("app").extend("system").extend("commands");

const createCommandBus = () => {
  const handlers = {};

  return {
    register(type, handler) {
      handlers[type] = handler;
    },
    call(type, data) {
      if (!handlers[type]) {
        commandLogger(`Command ${type} not found`);

        throw new Error(`Handler not found for type "${type}"`);
      }

      commandLogger(`Calling command ${type}`, data);

      return Promise.resolve(handlers[type](data || {})); // Making sure to be async - Monad
    }
  }
}

module.exports = { createCommandBus }
