const { createProducer } = require("./producer");
const { createConsumer } = require("./consumer");

const createKafka = ({ host, id }, topics, topicMapper) => {
  const producer = createProducer({ host, id }, topicMapper);
  const consumer = createConsumer({ host, id }, topics);

  return {
    register: consumer.register,
    send: producer.send,
  }
};

module.exports = { createKafka };
