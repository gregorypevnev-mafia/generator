// SEPARATE CLIENTS FOR PROEDUCING AND CONSUMING
const { ConsumerGroup } = require("kafka-node");
const debug = require("debug");

const kafkaConsumerLogger = debug("app").extend("system").extend("kafka").extend("consumer");

const createConsumer = ({ host, id }, topics) => {
  let listener = null; // Single listener (Design Choice) - ONLY USED BY EVENTS ANYWAY

  // Client, Requests (Initial Reads), Options
  const consumer = new ConsumerGroup({
    kafkaHost: host,

    groupId: id, // Each Service should be in its Consumer-Group (Avoiding duplication) 
    // -> Attach random ID when EACH Instance of Service should receive Record (WHY???)

    sessionTimeout: 10000, // At least one Heart-Beat needs to be received from other Consumers in the group every 10s

    fromOffset: "latest", // Read from the latest offset made by Consumer-Group Topic (Using the role of its memver)
    outOfRangeOffset: "latest", // No offset for a specific Partition for the Group is found -> Use Latest

    protocol: ["roundrobin"], // Distriuting Partitions over consumers in a Group using either Ranges or Round-Robin

    // Auto-Committing is BAD (Made at intervals + Unreliable)
    // BUT IT IS OK IN THIS CONTEXT (Node.js) - CANNOT BLOCK -> Commits are NOT reliable (Can commit smallest)
    //  => NO other choice
    autoCommit: true,

    autoCommitIntervalMs: 1000,
  }, topics);

  consumer.on("connect", () => {
    kafkaConsumerLogger("Consumer connected")
  });

  // IMPORTANT: Message needs to be received event if it is NOT processed - Committing 
  consumer.on("message", async message => {
    try {
      const type = message.key, { data, source } = JSON.parse(message.value);

      // Only processing if the message is NOT sent by the current service (AND LISTENER IS PROVIDED OF COURSE)
      //   + Type is present (NOT a System-Event otherwise)
      kafkaConsumerLogger("Incoming message:", type);

      if (type && source === id) kafkaConsumerLogger("Local Message - Ignoring");
      else if (!listener) kafkaConsumerLogger("External Message - Listener is NOT provided");
      else {
        kafkaConsumerLogger("External Message - Publishing an Event", type);
        await listener(type, data);
      }

      // SUPER-IMPORTANT: Do NOT commit immediately - Wait until a batch / group of messages is processed -> Commit afterwards
      //  - STILL NOT ENOUGH => AUTO-COMMIT
      // 
      // setImmediate(() => {
      //   consumer.commit((err, data) => {
      //     if (err) console.log("Could not commit", err);
      //     else console.log("Committed", data);
      //   }); // Commit after processing is done
      // });
    } catch (e) {
      kafkaConsumerLogger("Error processing message", message);

      throw e;
    }
  });

  return {
    register(newListerner) {
      listener = newListerner;
    }
  }
};

module.exports = { createConsumer };
