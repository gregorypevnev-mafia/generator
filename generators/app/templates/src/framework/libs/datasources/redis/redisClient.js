const redis = require("redis");
const asyncRedis = require("async-redis");

const createRedisClient = ({ host, port, password }) => {
  const config = { host, port };

  if (password) config.password = password;

  const client = redis.createClient(config);

  const asyncClient = asyncRedis.decorate(client);

  return asyncClient;
};


module.exports = { createRedisClient };