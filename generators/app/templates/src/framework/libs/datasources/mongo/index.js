const { createMongoDatasource } = require("./mongoDatasource");

module.exports = {
  name: "mongo",
  creator: createMongoDatasource,
};