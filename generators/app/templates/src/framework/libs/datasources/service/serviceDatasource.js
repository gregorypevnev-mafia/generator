const { createServiceClient } = require("./serviceClient");
const { provisionOperations } = require("../common");

const createServiceDatasource = (name, serviceConfig) => {
  const client = createServiceClient(serviceConfig);

  return {
    name,
    query(query) {
      return query(client);
    },
    repository(repo) {
      return provisionOperations(client, repo.operations);
    }
  }
};

module.exports = { createServiceDatasource };
