const express = require("express");
const parser = require("body-parser");
const cors = require("cors");
const morgan = require("morgan");

const createDefaultServer = () => {
  const app = express();

  app.use(morgan("combined"));
  app.use(cors());
  app.use(parser.json());

  return app;
}

module.exports = { createDefaultServer };