const { createTicketController } = require("./ticket");

const createController = tickets => {
  const ticketController = createTicketController(tickets);

  return {
    path: "/",
    use: ticketController,
  };
}

module.exports = { createController };
