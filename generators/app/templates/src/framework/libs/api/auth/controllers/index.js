const { createSignInController } = require("./signin");
const { createSignUpController } = require("./signup");
const { createSignOutController } = require("./signout");
const { createMeController } = require("./me");

const createAuthController = ({
  createUser,
  findUser,
  detailsForUser,
  encodeToken,
  attachToken,
  invalidateToken,
}) => {
  return {
    path: "/auth",
    use: [
      createSignInController({ findUser, encodeToken, attachToken }),
      createSignUpController({ createUser, encodeToken, attachToken }),
      createSignOutController({ invalidateToken }),
      createMeController({ detailsForUser }),
    ]
  }
};

module.exports = { createAuthController };
