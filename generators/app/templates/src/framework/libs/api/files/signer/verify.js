const { hashFile } = require("./hasher");

const createVerifier = ({ secret }) =>
  (filename, extension, user, expires, signature) => {
    const verificationSignature = hashFile({ filename, extension, secret, user, expires });

    if (verificationSignature !== signature) throw { message: "Invalid signature" };

    const timestamp = Date.now();

    if (timestamp > expires) throw { message: "Singature has expired" };
  };

module.exports = {
  createVerifier,
};
