const { createApi } = require("../libs/api/main");
const { createInfo } = require("../libs/api/info");
const { createValidation } = require("../libs/api/validation");
const { createWS } = require("../libs/api/ws");
const { createAuthentication } = require("../libs/api/auth");
const { createFiles } = require("../libs/api/files");

const loadApi = ({
  basePath,
  auth,
  ws,
}) => {
  const components = [];
  let createServer = null;

  return {
    addInfo(infoConfig) {
      components.push(createInfo(infoConfig));
    },
    addFiles(filesConfig) {
      components.push(createFiles(filesConfig));
    },
    addAuth(identityManagement, permissionsManagement) {
      components.push(createAuthentication(identityManagement, permissionsManagement, auth));
    },
    addValidation(schemas) {
      components.push(createValidation(schemas));
    },
    addWebSockets(tickets) {
      components.push(createWS(tickets, ws.mode));
    },
    addCustomApi(component) {
      components.push(component);
    },
    useServer(customServerPreparation) {
      createServer = customServerPreparation;
    },
    build(dependencies) {
      return createApi(components, dependencies, createServer, {
        apiPath: basePath,
        wsPath: ws.basePath,
      });
    }
  }
}

module.exports = { loadApi };
