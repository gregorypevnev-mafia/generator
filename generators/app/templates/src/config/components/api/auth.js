module.exports = {
  tokens: {
    algorithm: "HS256",
    audience: "APP",
    issuer: "SYSTEM",
    secret: "SECRET",
    session: 24 * 60 * 60, // Seconds, NOT milliseconds
  },
  http: {
    header: "Authorization",
    token: "JWT",
  }
};