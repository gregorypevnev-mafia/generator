module.exports = {
  basePath: "/api",

  auth: require("./auth"),

  ws: require("../common/ws"),
};