const MESSAGE_CHANNEL = "messages";
const CONTROL_CHANNEL = "control";

const CLOSE_MESSAGE = "close";
const INFO_MESSAGE = "info";
const JOIN_ROOM_MESSAGE = "join-room";
const LEAVE_ROOM_MESSAGE = "leave-room";

const CONNECTED_EVENT = "open-connection";
const DISCONNECTED_EVENT = "close-connection";

module.exports = {
  channels: {
    messageChannel: MESSAGE_CHANNEL,
    controlChannel: CONTROL_CHANNEL,
  },

  messages: {
    closeMessage: CLOSE_MESSAGE,
    infoMessage: INFO_MESSAGE,
    joinRoomMessage: JOIN_ROOM_MESSAGE,
    leaveRoomMessage: LEAVE_ROOM_MESSAGE,
  },

  events: {
    connectedEvent: CONNECTED_EVENT,
    disconnectedEvent: DISCONNECTED_EVENT,
  },

  rooms: {
    defaultRoom: "INFO-ROOM",
  },

  store: {
    redis: {
      port: Number("<%= port %>"),
      host: "<%= host %>",
      <% if(password) { %> password: "<%= password %>" <% } %>
    }
  },

tickets: {
  ttl: 10,  // In Seconds
  },

ws: require("./common/ws"),
};
