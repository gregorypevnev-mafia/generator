module.exports = {
  pipes: {
    input: require("./input"),
    output: require("./output"),
  },
  connection: require("./connection"),
};
