const extractQueryData = ({ user, room }) => {
  if (!user || !room) return null;

  return { user, room };
};

module.exports = extractQueryData;