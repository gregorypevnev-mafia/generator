const crypto = require("crypto");

const generateTicket = (user, room) =>
  crypto.createHash("sha256")
    .update(user)
    .update(room)
    .update(Date.now().toString())
    .digest("hex"); // Using HEX to avoid unsupported characters

const createTicket = ({ data, user }) => {
  const details = {
    user: user.id,
    room: data.room,
  };

  const ticket = generateTicket(user.id, data.room);

  return { ticket, details };
}

module.exports = createTicket;
