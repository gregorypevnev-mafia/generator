const Generator = require("yeoman-generator");
const { questionsForType } = require("../datasource/utils/questions");
const { isValidPath, formatPath } = require("../controller/paths");

const DEFAULT_CONNECTION_PATH = "/ws";

const DEFAULT_CONNECTION_MODE = "query";

class MessagingGenerator extends Generator {
  constructor(args, opts) {
    super(args, opts);

    this.conflicter.force = true;

    this.data = {
      redis: null,
      connection: {
        path: DEFAULT_CONNECTION_PATH,
        mode: DEFAULT_CONNECTION_MODE,
      }
    };
  }

  async promptingConnectionSettings() {
    this.log("Enter connection settings for Web-Sockets");

    this.data.connection = await this.prompt([
      {
        name: "path",
        type: "input",
        message: "Enter connection path for web sockets",
        default: DEFAULT_CONNECTION_PATH,
        validate(path) {
          return isValidPath(path);
        },
        filter(path) {
          return formatPath(path);
        }
      },
      {
        name: "mode",
        type: "list",
        message: "Enter connection mode",
        choices: [
          {
            name: "Query data",
            value: "query"
          },
          {
            name: "Connection ticket",
            value: "ticket"
          }
        ],
        default: DEFAULT_CONNECTION_MODE
      }
    ]);
  }

  async promptingRedisStore() {
    this.log("Enter Redis connection details for Pub-Sub");

    this.data.redis = await this.prompt(questionsForType("redis"));
  }

  writingMessaging() {
    this.fs.copy(
      this.templatePath("./messaging"),
      this.destinationPath("./src/app/messaging")
    );
  }

  writingConfiguration() {
    this.fs.copyTpl(
      this.templatePath("./config/messaging.js"),
      this.destinationPath("./src/config/components/messaging.js"),
      this.data.redis
    );
    this.fs.copyTpl(
      this.templatePath("./config/ws.js"),
      this.destinationPath("./src/config/components/common/ws.js"),
      this.data.connection
    );
  }
}

module.exports = MessagingGenerator;
