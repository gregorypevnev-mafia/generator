const name = "<%= name %>";
const datasource = "<%= datasource %>";
const transactional = <%= isTransactional %>;

module.exports = {
  name,
  datasource,
  transactional,
  operations: {

  },
};