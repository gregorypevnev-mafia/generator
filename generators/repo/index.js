const ComponentGenerator = require("../../helpers/componentGenerator");

const settings = {
  base: "repositories",
  message: "Enter name of the repository",
};

class RepoGenerator extends ComponentGenerator {
  constructor(args, opts) {
    super(args, opts, settings);
  }

  async promptingQueryName() {
    // SUPER-IMPORTANT: Await - Otherwise "Writing" methods are executed next
    await this.promptingName();
  }

  async promptingDatasource() {
    const answers = await this.prompt([
      {
        name: "datasource",
        type: "input",
        message: "Enter datasource which the repository should use",
        validate(ds) {
          return !!ds;
        }
      },
      {
        name: "isTransactional",
        type: "confirm",
        message: "Is repository transactional?",
        default: false,
      }
    ]);

    this.datasource = answers["datasource"];
    this.isTransactional = answers["isTransactional"];
  }

  configuringComponent() {
    this.configuringPaths();
  }

  writingComponent() {
    this.writingComponents();
    this.writingRequirements();
  }

  writingRepo() {
    this.fs.copyTpl(
      this.templatePath("./repo.js"),
      this._private_destination(),
      {
        name: this._private_name(),
        datasource: this.datasource,
        isTransactional: this.isTransactional,
      }
    );
  }
}

module.exports = RepoGenerator;