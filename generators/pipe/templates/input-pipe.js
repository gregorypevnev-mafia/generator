const name = "<%= messageName %>";

const pipe = ({ }) => ({ payload, user, room }) => {
  return {
    type: "<%= eventName %>",
    payload: {
      payload,
      user,
      room,
    }
  }
};

module.exports = {
  name,
  pipe,
};
