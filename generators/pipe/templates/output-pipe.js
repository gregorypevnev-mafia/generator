const name = "<%= eventName %>";

const pipe = ({ }) => ({ }) => {
  return {
    type: "<%= messageName %>",
    payload: {},
    // user: "", // Unicast
    // users: [], // Multicast
    // room: "", // Broadcast
    // Anycast by default
  }
};

module.exports = {
  name,
  pipe,
};
