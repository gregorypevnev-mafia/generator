const ComponentGenerator = require("../../helpers/componentGenerator");
const { paramCase } = require("param-case");

const INPUT = "input";
const OUTPUT = "output";

class CommandGenerator extends ComponentGenerator {
  _private_isMessagingReady() {
    return this.fs.exists(this.destinationPath("src/app/messaging/index.js"));
  }

  constructor(args, opts) {
    super(args, opts, {});
  }

  initializingPipe() {
    if (!this._private_isMessagingReady())
      throw new Error("Messaging not initialized");
    // Do NOT compose - Will not write requirement
  }

  async promptingPipeType() {
    this.pipeData = await this.prompt([
      {
        name: "type",
        type: "list",
        message: "Enter type of the pipe",
        choices: [INPUT, OUTPUT],
        default: INPUT,
      },
      {
        name: "eventName",
        type: "input",
        message: "Name of the event",
        validate(event) {
          return !!event;
        },
        filter(event) {
          return paramCase(event);
        }
      },
      {
        name: "messageName",
        type: "input",
        message: "Name of the message",
        validate(message) {
          return !!message;
        },
        filter(message) {
          return paramCase(message);
        }
      },
    ]);

    this.settings.base = `messaging/${this.pipeData.type}`;
  }

  configuringPipe() {
    this.configuringPaths();
  }

  writingPipe() {
    const { type, ...data } = this.pipeData;

    this.writingComponents();

    this.writingRequirements();

    this.fs.copyTpl(
      this.templatePath(type === INPUT ? "./input-pipe.js" : "output-pipe.js"),
      this._private_destination(),
      data
    );
  }
}

module.exports = CommandGenerator;