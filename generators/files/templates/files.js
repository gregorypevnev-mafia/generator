module.exports = {
  secret: "<%= secret %>",
  path: "<%= path %>",
  ttl: 60 * 1000,
  allowedExtensions: [],
  fileField: "<%= field %>",
}