const Generator = require("yeoman-generator");
const { isValidPath, formatPath } = require("../controller/paths");

const DEFAULT_STORAGE_PATH = "/data/files";
const DEFAULT_FILE_FIELD = "file";

class FilesGenerator extends Generator {
  constructor(args, opts) {
    super(args, opts);

    this.conflicter.force = true;

    this.data = {
      secret: "",
      path: DEFAULT_STORAGE_PATH,
      field: DEFAULT_FILE_FIELD,
    };
  }

  async promptingConnectionSettings() {
    this.data = await this.prompt([
      {
        name: "secret",
        type: "password",
        message: "Enter secret for providing signed URLs",
        validate(secret) {
          return !!secret;
        },
      },
      {
        name: "path",
        type: "input",
        message: "Enter storage path for the files",
        default: DEFAULT_STORAGE_PATH,
        validate(path) {
          return isValidPath(path);
        },
        filter(path) {
          return formatPath(path);
        }
      },
      {
        name: "field",
        type: "input",
        message: "Enter name of the field for uploading files",
        default: DEFAULT_FILE_FIELD,
        validate(field) {
          return !!field;
        },
      },
    ]);
  }

  writingFilesConfig() {
    this.fs.copyTpl(
      this.templatePath("./files.js"),
      this.destinationPath("./src/config/components/files.js"),
      this.data
    );
  }
}

module.exports = FilesGenerator;
