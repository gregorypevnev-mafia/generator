const ComponentGenerator = require("../../helpers/componentGenerator");

const settings = {
  base: "event-handlers",
  message: "Enter type of the event handler",
};

class HandlerGenerator extends ComponentGenerator {
  constructor(args, opts) {
    super(args, opts, settings);
  }

  async promptingHandlerName() {
    // SUPER-IMPORTANT: Await - Otherwise "Writing" methods are executed next
    await this.promptingName();
  }

  configuringComponent() {
    this.configuringPaths();
  }

  writingComponent() {
    this.writingComponents();
    this.writingRequirements();
  }

  writingHandler() {
    this.fs.copyTpl(
      this.templatePath("./handler.js"),
      this._private_destination(),
      { type: this._private_name() }
    );
  }
}

module.exports = HandlerGenerator;
