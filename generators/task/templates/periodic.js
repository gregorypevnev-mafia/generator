const auto = <%= isAuto %>;

const name = "<%= name %>";

// FLUSHALL to delete tasks
const params = {
  repeat: {
    every: <%= rate %>
  }
};

const processor = ({ }) => ({ }) => {

};

module.exports = {
  auto,
  name,
  params,
  processor,
};
