const auto = <%= isAuto %>;

const name = "<%= name %>";

// FLUSHALL to delete tasks
const params = {
  delay: <%= delay %>,
};

const processor = ({ }) => ({ }) => {

};

module.exports = {
  auto,
  name,
  params,
  processor,
};
