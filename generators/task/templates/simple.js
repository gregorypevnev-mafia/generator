const auto = <%= isAuto %>;

const name = "<%= name %>";

const params = {};

const processor = ({ }) => ({ }) => {

};

module.exports = {
  auto,
  name,
  params,
  processor,
};
