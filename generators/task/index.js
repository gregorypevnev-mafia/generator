const ComponentGenerator = require("../../helpers/componentGenerator");

const SIMPLE_TYPE = "Simple";
const DELAYED_TYPE = "Delayed";
const PERIODIC_TYPE = "Periodic";

const MANUAL_EXECUTION = "Manually";
const AUTO_EXECUTION = "Automatically";

const settings = {
  base: "tasks",
  message: "Enter name of the task",
};

class TaskGenerator extends ComponentGenerator {
  constructor(args, opts) {
    super(args, opts, settings);
  }

  initializingTask() {
    this.taskData = {
      name: "",
      isAuto: false,
    };
  }

  async promptingTaskName() {
    // SUPER-IMPORTANT: Await - Otherwise "Writing" methods are executed next
    await this.promptingName();

    this.taskData.name = String(this._private_name());
  }

  async promptingTaskType() {
    const answers = await this.prompt([
      {
        name: "type",
        type: "list",
        message: "Type of the task",
        choices: [SIMPLE_TYPE, DELAYED_TYPE, PERIODIC_TYPE],
        default: SIMPLE_TYPE
      }
    ]);

    this.type = answers["type"];
  }

  async promptingTaskDetails() {
    if (this.type === SIMPLE_TYPE) return;

    if (this.type === DELAYED_TYPE) {
      const answers = await this.prompt([
        {
          name: "delay",
          type: "number",
          message: "Delay of the task (ms)",
          validate(delay) {
            return delay > 0;
          },
          filter(delay) {
            return Number(delay);
          }
        }
      ]);

      this.taskData.delay = answers["delay"];

      return;
    }

    const answers = await this.prompt([
      {
        name: "rate",
        type: "number",
        message: "Rate of the task (ms)",
        validate(rate) {
          return rate > 0;
        },
        filter(rate) {
          return Number(rate);
        }
      }
    ]);

    this.taskData.rate = answers["rate"];
  }

  async promptingIfTaskIsAuto() {
    const answers = await this.prompt([
      {
        name: "execution",
        type: "list",
        message: "How should the task be executed?",
        choices: [MANUAL_EXECUTION, AUTO_EXECUTION],
        default: MANUAL_EXECUTION
      }
    ]);

    // TODO: Constants everywhere

    this.taskData.isAuto = answers["execution"] === AUTO_EXECUTION;
  }

  configuringComponent() {
    this.configuringPaths();
  }

  writingComponent() {
    this.writingComponents();
    this.writingRequirements();
  }

  writingCommand() {
    this.fs.copyTpl(
      this.templatePath(`./${this.type}.js`),
      this._private_destination(),
      this.taskData
    );
  }
}

module.exports = TaskGenerator;