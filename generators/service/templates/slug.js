const name = "<%= name %>";

const whitespace = new RegExp(" ", "g");
const specialChars = new RegExp("[!@#_]", "g");

const fromTitle = ({ }) => title =>
  String(title)
    .toLowerCase()
    .replace(whitespace, "-")
    .replace(specialChars, "");

module.exports = {
  name,
  functions: {
    fromTitle
  }
};