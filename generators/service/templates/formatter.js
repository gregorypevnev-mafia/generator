const name = "formatter";

const user = () => ({ password, ...details }) => details;

module.exports = {
  name,
  functions: {
    user,
  }
};