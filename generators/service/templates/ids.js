const { v1: uuid } = require("uuid");

const name = "<%= name %>";

const generate = ({ }) => () => String(uuid());

module.exports = {
  name,
  functions: {
    generate,
  }
};