const ComponentGenerator = require("../../helpers/componentGenerator");

const settings = {
  base: "services",
  message: "Enter name of the service",
};

class QueryGenerator extends ComponentGenerator {
  constructor(args, opts) {
    super(args, opts, settings);
  }

  async promptingServiceName() {
    // SUPER-IMPORTANT: Await - Otherwise "Writing" methods are executed next
    await this.promptingName();
  }

  async promptingServiceType() {
    const answers = await this.prompt([
      {
        name: "type",
        type: "list",
        message: "Enter type of the service",
        choices: ["blank", "ids", "password", "slug", "time", "formatter"],
        default: "blank"
      }
    ]);

    this.type = answers["type"];
  }

  configuringComponent() {
    this.configuringPaths();
  }

  writingComponent() {
    this.writingComponents();
    this.writingRequirements();
  }

  writingService() {
    this.fs.copyTpl(
      this.templatePath(`./${this.type}.js`),
      this._private_destination(),
      { name: this._private_name() }
    );
  }
}

module.exports = QueryGenerator;