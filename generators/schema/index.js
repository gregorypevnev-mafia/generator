const ComponentGenerator = require("../../helpers/componentGenerator");

const settings = {
  base: "schemas",
  message: "Enter name of the schema",
};

class SchemaGenerator extends ComponentGenerator {
  constructor(args, opts) {
    super(args, opts, settings);
  }

  async promptingSchemaName() {
    // SUPER-IMPORTANT: Await - Otherwise "Writing" methods are executed next
    await this.promptingName();
  }

  configuringComponent() {
    this.configuringPaths();
  }

  writingComponent() {
    this.writingComponents();
    this.writingRequirements();
  }

  writingCommand() {
    this.fs.copyTpl(
      this.templatePath("./schema.js"),
      this._private_destination(),
      { name: this._private_name() }
    );
  }
}

module.exports = SchemaGenerator;