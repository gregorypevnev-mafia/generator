const Generator = require("yeoman-generator");
const { paramCase } = require("param-case");
const esprima = require("esprima");
const escodegen = require("escodegen");

const PROJECT_ROOT = "./src/app";

const generateModule = () => escodegen.generate({
  "type": "Program",
  "body": [
    {
      "type": "ExpressionStatement",
      "expression": {
        "type": "AssignmentExpression",
        "operator": "=",
        "left": {
          "type": "MemberExpression",
          "computed": false,
          "object": {
            "type": "Identifier",
            "name": "module"
          },
          "property": {
            "type": "Identifier",
            "name": "exports"
          }
        },
        "right": {
          "type": "ArrayExpression",
          "elements": []
        }
      }
    }
  ],
  "sourceType": "module"
});

const generateRequirement = requirement => ({
  "type": "CallExpression",
  "callee": {
    "type": "Identifier",
    "name": "require"
  },
  "arguments": [
    {
      "type": "Literal",
      "value": `./${requirement}`,
      "raw": `'./${requirement}'`
    }
  ]
});

const deepEquals = (obj1, obj2) =>
  JSON.stringify(obj1).localeCompare(JSON.stringify(obj2)) === 0;

const addRequirement = (requirements, newRequirement) => {
  const elements = requirements.body[0].expression.right.elements;

  const isRequired = elements.find(
    requirement => deepEquals(requirement, newRequirement)
  );

  if (!isRequired) elements.push(newRequirement);
}

const componentMatcher = /^([A-Za-z0-9\-]+\/)?([A-Za-z0-9\-]+)(\.js)?$/;

const formatComponent = componentPath => {
  const [_, module, name, __] = componentPath.match(componentMatcher);

  return {
    name: paramCase(name),
    module: module ? paramCase(module.slice(0, module.length - 1)) : null,
  };
}

class ComponentGenerator extends Generator {
  _private_destination() {
    return this.data.paths.filePath;
  }

  _private_name() {
    return this.param;
  }

  _private_require(module, file) {
    const moduleContents = this.fs.read(module);

    const moduleRequirements = esprima.parseModule(moduleContents);

    addRequirement(moduleRequirements, generateRequirement(file));

    // Cool: Auto-Formatting
    const newModuleContents = escodegen.generate(moduleRequirements);

    this.fs.write(module, newModuleContents);
  }

  constructor(args, opts, { base, message }) {
    super(args, opts);

    this.argument("module", { type: String, required: true });

    // Overwrite files without asking - Otherwise Indexes will not be updatedd without prompt (Annoying)
    this.conflicter.force = true;

    this.settings = {
      base,
      message,
    };

    this.param = null;
  }

  async promptingName() {
    const answers = await this.prompt([
      {
        name: "name",
        type: "input",
        message: this.settings.message,
        validate(type) {
          return !!type;
        },
        filter(type) {
          return paramCase(type);
        }
      }
    ]);

    this.param = answers["name"];
  }

  // Configuring paths of modules and files AFTER prompting and initilizing - Gives extra flexibility
  configuringPaths() {
    const { name, module } = formatComponent(this.options["module"]);

    const basePath = this.destinationPath(`${PROJECT_ROOT}/${this.settings.base}/index.js`);
    const modulePath = module ? this.destinationPath(`${PROJECT_ROOT}/${this.settings.base}/${module}/index.js`) : null;
    const filePath = this.destinationPath(`${PROJECT_ROOT}/${this.settings.base}${module ? ("/" + module) : ""}/${name}.js`);

    this.data = {
      name,
      module,
      paths: {
        filePath,
        modulePath,
        basePath,
      }
    };
  }

  // Wriring AFTER Configuring / Prompting - Allowing changing settings in a flexible and dynamic way
  //  -> Prompting / Configuring / Validating before any files are written
  writingComponents() {
    const { paths: { modulePath, basePath } } = this.data;

    if (!this.fs.exists(basePath)) this.fs.write(basePath, generateModule());
    if (modulePath && !this.fs.exists(modulePath)) this.fs.write(modulePath, generateModule());
  }

  writingRequirements() {
    if (this.data.module) {
      this._private_require(this.data.paths.modulePath, this.data.name);
      this._private_require(this.data.paths.basePath, this.data.module);
    } else {
      this._private_require(this.data.paths.basePath, this.data.name);
    }
  }
}

module.exports = ComponentGenerator;